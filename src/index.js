const dob = "2022-02-22";

class Calculator {
  static dayPercentageValue;
  static daysOfAgeValue;

  static daysOfAge() {
    if (!this.daysOfAgeValue) {
      let today = new Date();
      today.setHours(0,0,0,0);
      this.daysOfAgeValue = Math.floor((today.getTime() - new Date(dob).getTime()) / (24 * 3600) / 1000);
    }
    return this.daysOfAgeValue;
  }

  static dayPercentage() {
    if (!this.dayPercentageValue) {
      this.dayPercentageValue = 100 / this.daysOfAge();
    }
    return `${this.dayPercentageValue.toFixed(2)}%`;
  }
}

const daysOfAge = Calculator.daysOfAge();
document.querySelectorAll('data.days-of-age').forEach(el => {
  el.innerHTML = daysOfAge;
  el.setAttribute('value', daysOfAge);
});


const dayPercentage = Calculator.dayPercentage();
document.querySelectorAll('data.day-percentage').forEach(el => {
  el.innerHTML = dayPercentage;
  el.value = dayPercentage;
});

setTimeout(() => {
  document.querySelectorAll('.day-width').forEach(el => {
    el.setAttribute('style', `width: ${Calculator.dayPercentage()};`);
  });
}, 100);
